= Running as a container

You can run any of the containers via any OCI-compliant runtime, such as `podman`, `docker`, etc.
Be sure to consult documentation for your runtime, e.g. https://podman.io/docs[Podman documentation].

The images are configured to launch systemd by default, but in order to just inspect
the system you will likely want to invoke e.g. `bash`:

```
$ podman run --rm -ti <yourimage> bash
bash-5.1# rpm -q kernel systemd podman bootc
kernel-5.14.0-432.el9.aarch64
systemd-252-32.el9.aarch64
podman-5.0.0-1.el9.aarch64
bootc-0.1.7-1.el9.aarch64
bash-5.1# 
```

If systemd is launched instead, you can enter the container using e.g. `podman exec`:

```
$ podman exec -ti wizardly_kare bash
```

to get an interactive shell (replace `wizardly_kare` with the actual container name).

TIP: The default `ENTRYPOINT` is `/sbin/init`, which will launch systemd. If you forgot
to specify `bash` above, you will likely be at a login prompt which is usually not
desired. Use the default `Ctrl-p Ctrl-q` detach sequence to disconnect the terminal,
and from there you can e.g. `podman rm -f` the container.

== Running without any additional privileges

At the current time, not all services are expected to run when executed without
any additional privileges, i.e. the defaults for `podman/docker run`.
This is a bug, albeit mostly a cosmetic one. For more information see
https://gitlab.com/fedora/bootc/tracker/-/issues/43[this tracking issue].

These services will run when the system is "deployed" to a virtual or physical machine,
or when the container is run safely with a few additional privileges and namespacing.

== Testing with additional privileges

It makes a lot of sense to "test" your operating system container *as a container*
before handing it off to more expensive CI/CD pipelines to provision virtual
or physical machines or upgrade in place. However, before you do this you
should carefully consider all of the below.

=== --privileged, user namespaces and /sys

Never use a "bare" `--privileged`. At a minimum, you should use `--privileged -v /sys:/sys:ro`.
This is documented in the systemd https://systemd.io/CONTAINER_INTERFACE/[container interface];
having a readonly `/sys` ensures that the udev in the container won't try to manage devices.

Additionally, you should ensure that the container is invoked in a user namespace.
When using an unprivileged runtime (e.g. podman as non-root) this is the default.
When running podman as root, use `--userns=auto`. For more information
see e.g. https://github.com/containers/podman/discussions/13728[this discussion].

=== Networking

You should never use `--net=host` with `default.target`. This will spawn e.g. `sshd` which will attempt to listen
on port 22 and such. When user namespacing is in use, this will just fail; if
you forget to set up a user namespace, this combination could cause the container
to try to interfere with the host services.

=== Block devices

Some tooling may want to create block devices (e.g. LVM). This will
be properly denied when the container is spawned in a user namespace.
In general you should not attempt to do this, but focus such testing
on virtual or physical machines.

