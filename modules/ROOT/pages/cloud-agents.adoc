= Understanding cloud agents

The base image does not contain any special hypervisor-specific agents.
The following specifically are not included for example:

- cloud-init
- vmware-guest-agent
- google-guest-agent
- qemu-guest-agent
- ignition
- afterburn

etc.

== Unnecessary on bare metal

For deployment to bare metal using e.g. Anaconda or `bootc install`, none of these are necessary.

== Can be installed as desired for cloud deployments

The goal of this project is to provide a generic base image; it is fully supported to install any desired cloud specific agents.
See

- xref:provisioning-aws.adoc[Provisioning AWS] for a cloud-init case

== Unnecessary for "immutable infrastructure" on hypervisors

A model we aim to emphasize is having the container image define the "source of truth" for system state.
This conflicts with using e.g. `cloud-init` and having it fetch instance metadata and raises questions around changes to the instance metadata and when they apply.

Related to this, `vmware-guest-agent` includes a full "backdoor" mechanism to log into the OS.

== Goal: containerized agents

A high level goal is also to aim for containerizing agents.

=== Specific agents

==== Ignition

Ignition as shipped by CoreOS Container Linux derivatives such as https://fedoraproject.org/en/coreos/[Fedora CoreOS] has a lot of advantages in providing a model that works smoothly across both bare metal and virtualized scenarios.

It also has some compelling advantages over cloud-init at a technical level. 

But just like cloud-init, there is also significant overlap between a container-focused model of the world and an Ignition-focused model.

Nevertheless, they don't actively conflict, and it is very likely that e.g. Fedora CoreOS will "rebase" on top of this project.
