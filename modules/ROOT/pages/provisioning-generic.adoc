= Installing on generic infrastructure

Not every deployment platform is explicitly covered by
this project. However, the aim of this project
is to support deployment anywhere that runs Linux
as a general rule.

== Using Anaconda for generic installations

Many virtualized environments can be configured to boot via an ISO (or PXE);
then the xref:bare-metal.adoc[Installing on bare metal] path can
be used to install.

== Using bootc-image-builder and raw files

The {bootc-image-builder}[bootc-image-builder] tool can generate e.g. `raw`
disk images from the container image, which can often be transformed
via additional tooling into a format suitable for a specific
virtualization platform.

For more, see xref:qemu-and-libvirt.adoc[qemu and libvirt].

== Using `bootc install to-existing-root`

The https://containers.github.io/bootc/bootc-install.html#using-bootc-install-to-existing-root[`bootc install to-existing-root`]
path supports installing "inside" an existing running Linux system, effectively
replacing its contents with the container image. This allows provisioning
systems using *existing* disk images already uploaded and managed
as part of the virtualization platform.