= Configuring container pull secrets

A common scenario is needing to configure the host system with a
"pull secret" necessary to fetch container images - which includes
the host updates itself!

There is a section in the upstream bootc documentation for
https://containers.github.io/bootc/building/secrets.html#secrets-eg-container-pull-secrets[secrets in general]
that applies here.

The following content applies to the *built image*. When using an external
installer such as Anaconda (xref:bare-metal.adoc[Bare metal]) or https://github.com/osbuild/bootc-image-builder[bootc-image-builder], those systems will each need
to be configured with any applicable pull secrets.

== Credential location for bootc

See https://github.com/ostreedev/ostree-rs-ext/blob/9a4743a657ffe0435018d9720c6df80a486ca0f1/man/ostree-container-auth.md[ostree-container-auth.md].

The recommendation for host `bootc` updates is to write configuration to `/etc/ostree/auth.json`
(which is shared with `rpm-ostree`). As of relatively recently, `/usr/lib/ostree/auth.json`
is also supported.

== Credential location for podman

See https://github.com/containers/image/blob/main/docs/containers-auth.json.5.md[containers-auth.json].

=== Lack of "system wide" podman credentials

A general conceptual problem with all of the available containers-auth locations
that are accepted by `podman` today is that the two locations are underneath:

- `/run`: This vanishes on reboot, which is not usually desired
- `/root`: Part of root's home directory, which is local *mutable* state by default

There is discussion about adding a system-wide location for the container stack,
but this has not yet happened. More in https://github.com/containers/image/pull/1746[this pull request].

== Unifying bootc and podman credentials

A common pattern will be using a single default global pull secret
for both bootc and podman.

The following container build demonstrates one approach to achieve this.

[subs="attributes"]
--
This reference example is maintained in {git-examples}/container-auth[container-auth].
--

.Containerfile
[source]
----
COPY link-podman-credentials.conf /usr/lib/tmpfiles.d/link-podman-credentials.conf
RUN --mount=type=secret,id=creds,required=true cp /run/secrets/creds /usr/lib/container-auth.json && \
    chmod 0600 /usr/lib/container-auth.json && \
    ln -sr /usr/lib/container-auth.json /etc/ostree/auth.json 
----

.link-podman-credentials.conf
[source]
----
# Make /run/containers/0/auth.json (a transient runtime file)
# a symlink to our /usr/lib/container-auth.json (a persistent file)
# which is also symlinked from /etc/ostree/auth.json.
d /run/containers/0 0755 root root -
L /run/user/0/containers/auth.json - - - - ../../../../usr/lib/container-auth.json
----
