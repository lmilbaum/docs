= Machine and image customization: generic vs building

A fundamental goal of this project is to make it easy
to create custom bootable Linux systems by building
container images.

However, not every use case needs or wants a specific
custom image; they can often use a "generic" image with
only a little bit of injected dynamic configuration.

One can use the term "base image" for an image such
as this one which is intended primarily to be used as
a derivation point, instead of being run directly.

The term we will use for the latter case to clearly distinguish
is "final image", again in contrast to "base image".

These two cases can also be further subdivided on an axis
of "generic" vs "specific" (to a use case).

A "generic" image is one that does not come with any
pre-set up configuration such as users, SSH keys, network
configuration, or applications.

A "specific" image in contrast is one explicitly designed
for a use case such as:

- Run a Postgres instance in a specific cloud
- Control software for a drone
- Be a graphical kiosk at a shopping mall

etc.

== "Final" images

Our sister project https://docs.fedoraproject.org/en-US/fedora-coreos/[Fedora CoreOS]
is an excellent example of a "final generic image". It
is "final" in the sense that while it *does* have an associated
container image, it is not currently the focus of the project
to derive from that image.

It is also generic - there's again no pre-set users or applications.
It focuses on Ignition as a generic customization mechanism
that writes to `/etc` and `/usr/local` and other directories
which act as machine-local mutable state.

== Customizing base images without deriving

The `/etc` directory is intended for configuration, and
many operations that one might include in a container build
are also viable to inject as machine-local state into
that directory (via kickstart, cloud-init, Ignition, etc).

For example, an organization may create a "final"
image derived from this project, but for one specific
site want to enable or disable one or more systemd units,
or inject an extra container image.

Or, the organization may want to produce a "final" image
but allow their own users to decide more dynamically
whether or not to enable FIPS mode.

Many tasks like this boil down to writing files into `/etc`,
which defaults to machine-local persistent, mutable state
partially to support this design.

In other words: It is supported today to use this project
to produce "generic" images, and will continue to be
into the future. However, the primary focus is on providing
tools to build custom derived images.
